<?php
$title = 'Vista de Productos'; 
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';
unset($_SESSION['product_id']);


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $var = filter_input(INPUT_GET, 'ids', FILTER_SANITIZE_STRING);
    if(isset($var)){
        $result = $productos_model->categoria_view($var);
    }
    else{
        $result = $productos_model->selectProductos();
    }
};

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $_SESSION['product_id'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        header('Location: /../carrito/cart_view.php');
};

?>
<html>
<!DOCTYPE html>
    <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://rawgit.com/jonmiles/bootstrap-treeview/master/dist/bootstrap-treeview.min.js"></script>
    </head>
        <div class="container col-sm-12">
            <div class="col-sm-2">
            <h1>Categorias</h1>
                <div id="treeview"></div>
            </div>
            <div class="col-sm-10">
                <?php
                    if($result):
                        foreach ($result as $prod) :
                            require './produc_info.php';
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
            
        </div>
        <script type="text/javascript" src="../assets/js/view.js"></script>
    </body>
    
</html>

