<?php
require_once '../Models/productos.php';
require_once '../shared/guard.php';
require_once '../shared/guard_acceso.php';
?>
<form method="POST" enctype="multipart/form-data" id="uploadForm">
    <div class="row-md-6">

      <div class="col-md-6">

         <div class="form-group">
          <label>Código:</label>
          <input type="number" name="codigo" id="codigo" class="form-control" min="0" required value="<?= $producto['codigo'] ?? '' ?>">
          <span class="text-danger"> <?=$nameErr;?></span>
        </div>

        <div class="form-group">
          <label>Nombre:</label>
          <input type="text" name="nombre" id="nombre" class="form-control" required value="<?= $producto['nombre'] ?? '' ?>">
        </div>

        <div class="form-group">
          <label>Descripción:</label>
          <textarea type="" name="descripcion" id="descripcion" class="form-control" rows="5" cols="50" required><?= $producto['descripcion'] ?? '' ?>
          </textarea>
        </div>

        <div class="form-group">
          <label>Categoria de la cual hereda:</label>
          <select class="form-control"  name="categoria" id="categoria" required value="<?= $producto['categoria'] ?? '' ?>">

          <?php If ($producto['categoria'] != 0)
          {
          ?>
           <option value="<?=$producto['categoria']?>"><?=$productos_model->categoria($producto['categoria'])?></option>
           <?=$productos_model->load_cbox($id)?>
          

          <?php } else {?>
            <?=$productos_model->load_cbox($id)?>
          <?php } ?>

          </select>
        </div>

        <div class="form-group">
          <label>Cantidad en Stock:</label>
          <input type="number" name="stock" id="stock" class="form-control" min="0" required value="<?= $producto['stock'] ?? '' ?>">
        </div>

        <div class="form-group">
          <label>Precio:</label>
          <input type="number" name="precio" id="precio" class="form-control" min="0" required value="<?= $producto['precio'] ?? '' ?>">
        </div>

        <div class="form-group">

          <input type="submit" name="btnRegistrar" id="btnRegistrar" class="btn btn-primary" value="Registrar">
        
        </div>

      </div>

    </div>  

    <div class="row-md-6">

      <div class="col-md-6">

        <div class="form-group">
        <label>Imagen:</label><br>
        <input type="file" name="imagen" id="file" accept="image/*" class="form-control">
        <br>
          <div class="col-md-12">

            <?php If ($producto['imagen'] != "")
            {
            ?>
            <img id="img" src="<?=$producto['imagen']?>" class="img-responsive center-block">
            <?php } else {?>
              <img id="img" src="<?=$img?>" class="img-responsive center-block">
            <?php } ?>

          </div>
        <script>
            function filePreview(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                      $('#img').attr('src', e.target.result);
                      /*$('#uploadForm + img').remove();
                      $('#uploadForm').after('<img src="'+e.target.result+'" width="540" height="400"/>');*/
                  }   
                  reader.readAsDataURL(input.files[0]);
              }
          }   

          $("#file").change(function () {
              filePreview(this);
          });     
        </script>
        
        </div>
        

      </div>
    
    </div>
  
</form>

