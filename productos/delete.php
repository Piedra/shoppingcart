<?php
//require_once '../shared/guard.php';
$title = 'Eliminar Producto';
require_once '../shared/header.php';
require_once '../shared/guard.php';
require_once '../shared/guard_acceso.php';

$producErr = "";

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $productos_model->find_categoria($id);
    $producErr = $productos_model->delete_prod($id);

    if ($producErr == "") {
      $productos_model->delete($id);
      return header('Location: /productos');
    }
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Está seguro de eliminar el producto con el id <?=$id?>?</p>
  <form method="POST">
    <input class="btn btn-primary" type="submit" value="Aceptar">
    <a class="btn btn-danger" href="/productos">Cancelar</a>
    <br>
    <span class="error"> <?=$producErr;?></span>
  </form>
</div>