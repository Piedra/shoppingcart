<?php
//require_once '../shared/guard.php';
$title = 'Editar productos';
require_once '../shared/header.php';
require_once '../shared/db.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$producto = $productos_model->find($id);
$img = '../assets/img/noimg.png';
$nameErr = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $codigo      = filter_input(INPUT_POST, 'codigo', FILTER_SANITIZE_STRING);
    $nombre      = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
    $descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
    $categoria   = filter_input(INPUT_POST, 'categoria', FILTER_SANITIZE_STRING);
    $stock       = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
    $precio      = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);

    if (isset($_POST['btnRegistrar'])) {
      $archivo = $_FILES['imagen']['tmp_name'];
      $destino = "../assets/img/". $_FILES['imagen']['name'];
      move_uploaded_file($archivo, $destino);
    }

    $imagen      = $destino ?? $img;
    if ($imagen == "../assets/img/") {
      $imagen = $producto['imagen'];
    } else {
      $imagen = $destino;
    }
    $nameErr = $productos_model->select_codU($codigo, $id);
    echo $codigo;
    var_dump($productos_model->search_codigo($id));
    if ($nameErr == "") {
      $productos_model->update($id, $codigo, $nombre, $descripcion, $categoria, $stock, $precio, $imagen);
      return header('Location: /productos');
    }
    
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once './form.php'; ?>
</div>