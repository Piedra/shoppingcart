<?php
//fetch.php

$title = 'Info';  
require_once '../shared/guard.php';
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$id_prod = filter_input(INPUT_POST, 'id_prod', FILTER_SANITIZE_STRING);
$id_usu = filter_input(INPUT_POST, 'id_usu', FILTER_SANITIZE_STRING);
$cantidad = filter_input(INPUT_POST, 'cant', FILTER_SANITIZE_STRING);
$precio = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);
$monto = $precio*$cantidad;

$result = $productos_model->select();
foreach($result as $row)
{
    $sub_data["id"] = $row["id"];
    $sub_data["name"] = $row["nombre"];
    $sub_data["text"] = $row["nombre"];
    $sub_data["categoria"] = $row["categoria"];
    $sub_data["href"] = "__DIR__ . /../productsview.php?ids=". $row["id"];
 $data[] = $sub_data;
}
foreach($data as $key => &$value)
{
 $output[$value["id"]] = &$value;
}
foreach($data as $key => &$value)
{
 if($value["categoria"] && isset($output[$value["categoria"]]))
 {
  $output[$value["categoria"]]["nodes"][] = &$value;
 }
}
foreach($data as $key => &$value)
{
 if($value["categoria"] && isset($output[$value["categoria"]]))
 {
  unset($data[$key]);
 }
}
echo json_encode($data);

?>