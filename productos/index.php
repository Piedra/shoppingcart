<?php
  $title = 'Productos';
  require_once '../shared/header.php';
require_once '../shared/guard.php';
require_once '../shared/guard_acceso.php';
require_once '../shared/db.php';
?>
<div class="container">
  <h1><?=$title?></h1>
  <table class="table table-striped table-bordered">
    <tr>
      <th>Codigo</th>
      <th>Nombre</th>
      <th>Descripcion</th>
      <th>Categoria</th>
      <th>Stock</th>
      <th>Precio</th>
      <th>Imagen</th>
      <th class="text-center">
        <a class="btn btn-success" href="/productos/create.php">+</a>
      </th>
    </tr>
<?php
$productos = $productos_model->read();

if ($productos) {
    foreach ($productos as $producto) {
        require './row.php';
    }
}
?>
  </table>
</div>
