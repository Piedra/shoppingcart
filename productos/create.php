<?php 
 $title = 'Registrar Productos'; 
require_once '../shared/header.php';
require_once '../shared/guard.php';
require_once '../shared/guard_acceso.php';
require_once '../shared/sessions.php';
require_once '../Models/productos.php';
require_once '../shared/db.php';
require_once '../php_database/PgConnection.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$producto = $productos_model->find($id);

$nameErr = "";

$img = '../assets/img/noimg.png';
if(isset($_POST['codigo'],$_POST['nombre'],$_POST['descripcion'],$_POST['categoria'],
$_POST['stock'],$_POST['precio']))
{   
  $codigo      = filter_input(INPUT_POST, 'codigo', FILTER_SANITIZE_STRING);
  $nombre      = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
  $descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
  $categoria   = filter_input(INPUT_POST, 'categoria', FILTER_SANITIZE_STRING) ?? '';
  $stock       = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
  $precio      = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);

  if (isset($_POST['btnRegistrar'])) {
    $archivo = $_FILES['imagen']['tmp_name'];
    $destino = "../assets/img/". $_FILES['imagen']['name'];
    move_uploaded_file($archivo, $destino);
  }
  $imagen      = $destino ?? $img;
  if ($imagen == "../assets/img/") {
    $imagen = $img;
  } else {
    $imagen = $destino;
  }
  $nameErr = $productos_model->select_codC($codigo);
  if ($nameErr == "") {
    $result = $productos_model->register_products($codigo,$nombre,$descripcion,$categoria,$stock,$precio,$imagen);
    if($result)
    {
      header('Location: /productos');
    }
  }
}

?>
<div class="container">

  <h1><?=$title?></h1>
  <?php require_once './form.php';?>

</div>