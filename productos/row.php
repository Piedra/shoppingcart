<?php
require_once '../Models/productos.php';
?>
<tr>
  <td><?=$producto['codigo']?></td>
  <td><?=$producto['nombre']?></td>
  <td><?=$producto['descripcion']?></td>
  <td><?=$productos_model->categoria($producto['categoria'])?></td>
  <td><?=$producto['stock']?></td>
  <td><?=$producto['precio']?></td>
  <td><img src="<?=$producto['imagen']?>" width="100" height="80"></td>
  <td>
    <a class="btn btn-warning" href="/productos/update.php?id=<?=$producto['id']?>">Editar</a>
    <br>
    <br>
    <a class="btn btn-danger" href="/productos/delete.php?id=<?=$producto['id']?>">Eliminar</a>
  </td>
</tr>
