<?php 
require_once __DIR__ . '/sessions.php';
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../">Lazy Cat</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li id="item" class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Categorias
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        /*
                        foreach  ($categorias as $value => $key) {
                            echo "<a class='dropdown-item' href='$value'>$value</a>";
                        }
                         */
                    ?>
                    <a name="categoria" class="dropdown-item">Action</a>
                    <a name="categoria" class="dropdown-item">Another action</a>
                </div>
            </li>
            <li id="right" class="nav-item" >
                <a class="nav-link" href="/../productos/productsview.php">Productos</a>
            </li>
            <form method="POST" class="form-inline my-2 my-lg-0" >
                <input id="searchBar" name = "searchBar" style="width:500px" class="form-control mr-sm-2" type="search" 
                placeholder="Search" aria-label="Search">
                <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
            </form>

        </ul>
        <?php if(isset($_SESSION['usuario'])): ?>
            <div class="items">
            <ul class="nav navbar-nav navbar-right">
            <li id="right2" class="nav-item" >
                <a id="btn_cart" class="nav-link" style="font-size: 20px" href="../carrito/cart.php"><i class="fas fa-shopping-basket"></i></a>
                <!-- Carrito-->
            </li> 
            <?php if($_SESSION['usuario'][7]=="f"): ?>
            <li id="right2" class="nav-item" >
                <a id="btn_cart" class="nav-link" style="font-size: 20px" href="../facturas/facturas.php"><i class="fas fa-file-invoice"></i></a>
                <!-- Factura-->
            </li>    
            <?php endif; ?>
            <?php if($_SESSION['usuario'][7]=="t"): ?>
            <li id="right2" class="nav-item" >
                <a id="btn_cart" class="nav-link" style="font-size: 20px" href="../productos/index.php"><i class="fas fa-file-signature"></i></a>
                <!-- Factura-->
            </li>    
            <?php endif; ?>
            <li id="right" class="nav-item" >
                <a class="nav-link" style="font-size: 20px" href="/../logout.php"><?php $_SESSION['usuario'][1] ?><i class="fas fa-sign-out-alt"></i></a>
                <!-- Cerrar Sesion-->
            </li> 
        </ul>
            </div>
        <?php else: ?>
        <ul class="nav navbar-nav navbar-right">
            <li id="right2" class="nav-item" >
                <a class="nav-link" href="../login.php">Inicia Sesion</a>
            </li>
            <li id="right" class="nav-item" >
                <a class="nav-link" href="./registrar.php">Registrate</a>
            </li>
        <?php endif; ?>
        </ul>
    </div>
</nav>