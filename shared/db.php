<?php

require_once __DIR__ . '/../php_database/PgConnection.php';
require_once __DIR__ . '/../Models/productos.php';
require_once __DIR__ . '/../Models/usuario.php';
require_once __DIR__ . '/../Models/carrito.php';
require_once __DIR__ . '/../Models/factura.php';

use Db\PgConnection;
//$con = new PgConnection('postgres', '4*231515', 'shoppingcart', 5432, 'localhost');
$con = new PgConnection('postgres', 'postgres', 'shoppingcart', 5432, 'localhost');
$con->connect();

$productos_model = new Models\productos($con);
$usuario_model = new Models\usuario($con);
$carrito_model = new Models\carrito($con);
$factura_model = new Models\factura($con);
