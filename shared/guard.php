<?php
require_once __DIR__ . '/sessions.php';
if (!isset($_SESSION['usuario']) || empty($_SESSION['usuario'])) {
    return header('Location: ../login.php');
}
$id_usuario_g = $_SESSION['usuario'][0];
$es_admin = $_SESSION['usuario'][7];