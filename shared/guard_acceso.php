<?php
require_once __DIR__ . '/sessions.php';
if (!isset($_SESSION['usuario']) || empty($_SESSION['usuario'])) {
    return header('Location: ../login.php');
}
$es_admin = $_SESSION['usuario'][7];

if($es_admin=="f"){
    return header('Location: ../extras/redireccion.php');
}