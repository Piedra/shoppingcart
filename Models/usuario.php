<?php

namespace Models {
      class Usuario {

            private $connection;
            public function __construct($connection) {
                $this->connection = $connection;
            }
    
            public function login($email,$password) {
              $result = $this->connection->runQuery('SELECT * FROM usuarios where correo = $1 and password = md5($2)', [$email,  $password]);
              return $result;
            }

            public function registrar($nombre,$apellidos,$direccion,$telefono,$correo,$password,$admin) {
                  $result = $this->connection->runStatement('INSERT INTO public.usuarios(nombre, apellidos, direccion, telefono, correo, password, administrador)
                  VALUES ($1, $2, $3, $4, $5, md5($6), $7) RETURNING id',[$nombre,$apellidos,$direccion,$telefono,$correo,$password,$admin]);
                  return $result;
            }

            public function cantidad() {
                  $result = $this->connection->runQuery('SELECT count(*) FROM usuarios', []);
                  return $result[0];
            }

            public function validacion($correo) {
                  $result = $this->connection->runQuery('SELECT * FROM usuarios WHERE correo = $1', [$correo]);
                  return $result;
            }

      }
}