<?php
namespace Models {
      class Carrito {

            private $connection;
            public function __construct($connection) {
                $this->connection = $connection;
            }
    

            public function select($id) {
              $result = $this->connection->runQuery('SELECT p.id, p.codigo, p.nombre, p.descripcion, p.precio, c.id as pro_id ,p.imagen ,p.stock, SUM(c.cantidad) as cantidad, SUM(c.monto) as monto
              FROM productos as p, cart as c, usuarios as u
              WHERE c.id_usu = $1 AND p.id = c.id_prod AND u.id = c.id_usu
              GROUP BY p.id, c.id', [$id]);
              return $result;
            }

            public function ingresar($id_prod,$id_usu,$cantidad,$monto) {
                  $result = $this->connection->runStatement('INSERT INTO public.cart(
                        id_prod, id_usu, cantidad, monto)
                        VALUES ($1, $2, $3,$4);',[$id_prod,$id_usu,$cantidad,$monto]);
                  return $result;
            }

            public function delete($id,$usu) {
                  $result = $this->connection->runStatement('DELETE FROM public.cart
                  WHERE id_prod = $1 AND id_usu = $2',[$id,$usu]);
                  return $result;
            }

            public function update($id,$cant,$monto) {
                  $result = $this->connection->runStatement('UPDATE public.cart
                  SET cantidad= $2, monto= $3
                  WHERE id = $1 ',[$id,$cant,$monto]);
                  return $result;
            }
            public function total() {
                  $result = $this->connection->runQuery('SELECT SUM(monto) AS total from cart',[]);
                  return $result;
            }

            public function deleteAll() {
                  $result = $this->connection->runQuery('DELETE FROM cart',[]);
                  return $result;
            
            }

            public function cantidad() {
                  $result = $this->connection->runQuery('SELECT SUM(stock) FROM factura',[]);
                  return $result[0];
            }

            public function montoTotal() {
                  $result = $this->connection->runQuery('SELECT SUM(monto) FROM factura',[]);
                  return $result[0];
            }

            public function checkout() {
                  $result = $this->connection->runQuery('INSERT INTO factura(usuario_id, monto, stock, nombre,precio)
                  SELECT id_usu,monto,cantidad,nombre,precio FROM cart AS c, productos AS p
                  WHERE c.id_prod = p.id',[]);
                  return $result;
            } 
            public function exist($id) {
                  $result = $this->connection->runQuery('SELECT id from cart where id_prod = $1',[$id]);
                  return $result;
            }
            public function updateCant($id,$cantidad) {
                  $result = $this->connection->runQuery('UPDATE public.productos
                  SET stock = stock - $2
                  WHERE id = $1;',[$id,$cantidad]);
                  return $result;
            }
            public function masVendido() {
                  $result = $this->connection->runQuery('SELECT SUM(f.stock) as stock, f.nombre FROM factura as f
                  GROUP BY f.nombre');
                  return $result;
            }
            
            public function productoMasComprado($id) {
                  $result = $this->connection->runQuery('SELECT SUM(stock) FROM factura 
                  WHERE usuario_id = $1', [$id]);
                  return $result[0];
            }

            public function montoId($id) {
                  $result = $this->connection->runQuery('SELECT SUM(monto) FROM factura 
                  WHERE usuario_id = $1', [$id]);
                  return $result[0];
            }
      }
}