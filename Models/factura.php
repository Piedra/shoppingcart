<?php

namespace Models {
      class Factura {

            private $connection;
            public function __construct($connection) {
                $this->connection = $connection;
            }

            public function busqueda($id)
            {
              $result = $this->connection->runQuery('SELECT  fecha , SUM(monto)as monto,SUM(stock) FROM factura where usuario_id = $1
              GROUP BY fecha' , [$id]);
              return $result;
            }
            
            public function detalles($fecha, $id)
            {
              $result = $this->connection->runQuery('SELECT f.nombre, f.precio, f.stock, f.monto 
              FROM factura as f
              WHERE f.usuario_id = $2 AND f.fecha = $1', [$fecha,$id]);
              return $result;
            }
            public function cronjob()
            {
              $result = $this->connection->runQuery('SELECT p.nombre, p.stock, p.codigo FROM productos as p WHERE  p.stock <= 3');
              return $result;
            }
         
         }
}