<?php
namespace Models {

  class Productos {
       
    private $connection;
    public function __construct($connection)
    {
      $this->connection = $connection;
    }
    
    public function select()
    {
      $result = $this->connection->runQuery('SELECT * FROM productos');
      return $result;
    }

    public function selectProductos()
    {
      $result = $this->connection->runQuery('SELECT * FROM productos WHERE stock > 0 ');
      return $result;
    }

    public function select_codC($codigo)
    {
      $res = "";
      $result = $this->connection->runQuery('SELECT * FROM productos ORDER BY codigo');
      foreach ($result as $value) {
        if ($codigo == $value['codigo']) {
          $res = "Este codigo '".$codigo. "', ya existe, por favor digite otro";
        }
      }
      return $res;
    }

    public function select_codU($codigo, $id)
    {
      $res = "";
      $result = $this->connection->runQuery('SELECT * FROM productos ORDER BY codigo');
      foreach ($result as $value) {
        if ($codigo == $value['codigo']) {
          $res = "Este codigo '".$codigo. "', ya existe, por favor digite otro";
        }
        if ($codigo == $this->search_codigo($id)) {
          $res = "";
        }
      }
      return $res;
    }

    public function delete_prod($id)
    {
      $res = "";
      $result = $this->connection->runQuery('SELECT * FROM productos ORDER BY id');
      foreach ($result as $value) {
        if ($id == $value['categoria']) {
          $res = $value['nombre']. "', no puede ser eliminado debido a que se encuentra asociado como categoria a otros producos";
        }
      }
      return $res;
    }

    public function search_codigo($id)
    {
      $result = $this->connection->runQuery('SELECT codigo FROM productos WHERE id = $1',[$id]);
      foreach ($result as $value) {
        return $value['codigo'];
      }
    }

    public function search_id($id)
    {
      $result = $this->connection->runQuery('SELECT id, codigo, nombre, descripcion, imagen, categoria, stock, precio FROM productos WHERE id = $1',[$id]);
      return $result[0];
    }
    
    public function find($id)
    {
      return $this->connection->runQuery('SELECT * FROM productos WHERE id = $1', [$id])[0];
    }

    public function find_categoria($id)
    {
      return $this->connection->runQuery('SELECT categoria FROM productos WHERE id = $1', [$id])[0];
    }


    public function update($id, $codigo, $nombre, $descripcion, $categoria, $stock, $precio, $imagen)
    {
      $this->connection->runStatement('UPDATE productos SET codigo=$2, nombre=$3, descripcion=$4, categoria=$5, stock=$6, precio=$7, imagen=$8 WHERE id = $1', [$id, $codigo, $nombre, $descripcion, $categoria, $stock, $precio, $imagen]);
    }

    public function delete($id)
    {
      $this->connection->runStatement('DELETE FROM productos WHERE id = $1', [$id])[0];
    }

    public function read()
    {
      return $this->connection->runQuery('SELECT * FROM productos ORDER BY id');
    }

    public function register_products($codigo,$nombre,$descripcion,$categoria,$stock,$precio,$imagen) {
      $result = $this->connection->runStatement('INSERT INTO productos(codigo, nombre, descripcion, categoria, stock, precio, imagen)
      VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id',[$codigo,$nombre,$descripcion,$categoria,$stock,$precio,$imagen]);
      return $result;
    }

    // Se hace el selec por condicion de categoria 0, porque son los productos que son padres
    public function load_combobox($id) {
      $result = $this->connection->runQuery('SELECT * FROM productos  WHERE categoria = 0 AND id != $1', [$id]);
      echo "<option value=0>"."Ninguna"."</option>";
      foreach ($result as $fila) {
        echo "<option value=".$fila['id'].">".$fila['nombre']."</option>";
      }
    }
    // DES-DOCUMENTAR EN CASO DE QUE LAS CATEGORIAS SOLO HEREDAN UNA VEZ
    //public function load_cbox($id) {
    //  $result = array();
    //  if ($id == "") {
    //    $result = $this->connection->runQuery('SELECT * FROM productos  WHERE categoria = 0');
    //  }elseif ($id != "") {
    //    $result = $this->connection->runQuery('SELECT * FROM productos  WHERE categoria = 0 AND id != $1', [$id]);
    //  }
    //  echo "<option value=0>"."Ninguna"."</option>";
    //  foreach ($result as $fila) {
    //    echo "<option value=".$fila['id'].">".$fila['nombre']."</option>";
    //  }
    //}

    public function load_cbox($id) {
      $result = array();
      if ($id == "") {
        $result = $this->connection->runQuery('SELECT * FROM productos');
      }elseif ($id != "") {
        $result = $this->connection->runQuery('SELECT * FROM productos  WHERE id != $1', [$id]);
      }
      echo "<option value=0>"."Ninguna"."</option>";
      foreach ($result as $fila) {
        echo "<option value=".$fila['id'].">".$fila['nombre']."</option>";
      }
    }

    public function categoria($categoria) {
      if ($categoria == 0) {
        return "Ninguna";
      }
      $result = $this->connection->runQuery('SELECT nombre FROM productos WHERE id = $1', [$categoria])[0];
      foreach ($result as $fila) {
        return $fila;
      }
    }

    public function categoria_view($id) {
      $result = $this->connection->runQuery('SELECT * FROM productos WHERE id = $1', [$id]);
      return $result;
    }
  }
}