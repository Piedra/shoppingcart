<?php 
$title = 'Facturas';
require_once '../shared/header.php';
require_once '../shared/guard.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';

//Se Evita que el admin entre
//if($es_admin=="t"){
//    header('Location: ./index.php');
//}

?>

<div class="container">
       <div class="col-sm-6 charts">
         <h1>Facturas</h1>
          <table id="tabla-ajax" class="table">
            <tr>
              <th style="text-align: center">Fecha</th>
              <th style="text-align: center">Monto</th>
            </tr>
          </table>
        </div>
        <div class="col-sm-6 charts">
         <h1>Detalles</h1>
          <table id="tabla-detallada" class="table">
            <tr>
                <th style="text-align: center">Nombre</th>
                <th style="text-align: center">Precio</th>
                <th style="text-align: center">Cantidad</th>
                <th style="text-align: center">Monto</th>
              </tr>
          </table>
        </div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../assets/js/factura.js"></script>
</div>



