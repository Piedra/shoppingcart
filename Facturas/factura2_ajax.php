<?php 
$title = 'Carrito';
require_once '../shared/guard.php';
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$fecha        = filter_input(INPUT_GET, 'fecha', FILTER_SANITIZE_STRING);
$result = $factura_model->detalles($fecha,$id_usuario_g);

echo json_encode($result);

