$(document).ready(function() {
  function params() {
    return {
      id: $("#id").val()
    };
  }

  function ajaxRequest() {
    return $.getJSON("/facturas/factura_ajax.php");
  }

  function ajaxRequestDos(fecha) {
    return $.getJSON("/facturas/factura2_ajax.php", { fecha: fecha });
  }

  function dibujarTabla() {
    var tabla = $("#tabla-ajax");
    tabla.find("tr:gt(0)").remove();
    ajaxRequest().then(function(json) {
      json.forEach(function(item) {
        tabla.append(`<tbody>
            <tr>
                <td>${item.fecha}</td>
                <td>${item.monto}</td>
                <td><button id="${
                  item.fecha
                }" style="width:100%" type="info" class="informar btn btn-info"><i class="fas fa-info"></i></button></td>
            </tr>
          </tbody>`);
      });
    });
  }

  $(document).on("click", ".informar", function(e) {
    var fecha = $(this).attr("id");
    var tabla = $("#tabla-detallada");
    tabla.find("tr:gt(0)").remove();
    ajaxRequestDos(fecha).then(function(json) {
      json.forEach(function(item) {
        tabla.append(`<tbody>
            <tr>
            <td>${item.nombre}</td>
            <td>${item.precio}</td>
            <td>${item.cantidad}</td>
            <td>${item.monto}</td>
            </tr>
          </tbody>`);
      });
    });
  });

  dibujarTabla();
});
