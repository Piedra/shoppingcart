var request;

// Bind to the submit event of our form
$("#foo").submit(function(event) {
  event.preventDefault();

  if (request) {
    request.abort();
  }
  var $form = $(this);

  var $inputs = $form.find("input, select, button, textarea");

  var serializedData = $form.serialize();

  $inputs.prop("disabled", true);

  if ($("#id_usu").val() == 0) {
    swal({
      title: "No has iniciado sesión?",
      text: "Para añadir objetos a tu carrito, primero debes ingresar :( ",
      icon: "info",
      buttons: ["Salir", "Iniciar Sesión"]
    }).then(willDelete => {
      if (willDelete) {
        location.href = "../login.php";
      } else {
        swal("Puedes Seguir navegando por nuestros productos :) !");
        setTimeout(() => {
          location.href = "../productos/productsview.php";
        }, 1500);
      }
    });
  } else {
    var id_prd = $("#id_prod").val();
    var id_usu = $("#id_usu").val();
    $.getJSON("/carrito/cart_ajax.php", { id: id_usu }, function(allData) {
      var repe = false;
      if (allData.length) {
        allData.forEach(function(item) {
          if (item.id == id_prd) {
            repe = true;
          }
        });
        if (repe) {
          swal({
            title: "Este Producto se encuentra en Carrito!",
            text:
              "Si desea cambiar la cantidad de este producto Por Favor Dirijase al Carrito",
            icon: "error",
            button: "Esta bien!"
          });
        } else {
          request = $.ajax({
            url: "../carrito/cart_view_ajax.php",
            type: "post",
            data: serializedData
          });
          swal({
            title: "Buena Eleccion !",
            text: "El Objeto A sido añadido al carrito!",
            icon: "success",
            button: "Oh si!"
          });
        }
      } else {
        request = $.ajax({
          url: "../carrito/cart_view_ajax.php",
          type: "post",
          data: serializedData
        });
        swal({
          title: "Buena Eleccion !",
          text: "El Objeto A sido añadido al carrito!",
          icon: "success",
          button: "Oh si!"
        });
      }
      console.log(repe);
    });
  }
});
