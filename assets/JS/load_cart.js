$(document).ready(function() {
  function params() {
    return {
      id: $("#id").val()
    };
  }

  function ajaxRequest() {
    return $.getJSON("/carrito/cart_ajax.php", params());
  }
  function ajaxRequestTotal() {
    return $.getJSON("/carrito/total_ajax.php");
  }

  function dibujarTabla() {
    var tabla = $("#tabla_productos");
    var tot = 0;
    tabla.find("tr:gt(0)").remove();
    ajaxRequest().then(function(json) {
      json.forEach(function(item) {
        var a = item.pro_id;
        tabla.append(`<tbody>
        <tr>
          <td>${item.codigo}</td><td>${item.nombre}</td>
          <td>${item.descripcion}</td><td>
          <input type="number" min="1" style="text-align: center" class=" form-control" value="${
            item.cantidad
          }" id="cantidad_producto${+a}"><input type="hidden" style="text-align: center" class=" form-control" value="${item.precio}" id="producto_precio${+a}">
          </td>
          <td>${item.precio}</td>
          <td><img src="${item.imagen}" width="70" height="70"></td><td><input type="text" readonly value="${item.cantidad * item.precio}" 
          id="monto${+a}" class="monto form-control"></td>
          <td><button value ="${
            item.stock
          }" id ="${item.pro_id}" type="btn" class="editar btn btn-warning"><i class="fas fa-edit"></i></i></button></td>
          <td><button id="${
            item.id
          }" type="btn"class="eliminar btn btn-danger"><i class="fas fa-minus-square"></i></button></td>
          
          </tr>
        </tbody>`);
      });
    });
    preciototal();
  }

  $(document).on("click", ".eliminar", function(e) {
    var id = $(this).attr("id");
    //Agregar una Alerta
    swal({
      title: "Seguro que deseas eliminar este Objeto?",
      text: "",
      icon: "warning",
      buttons: ["Cancelar", "Eliminar"],
      danger: true
    }).then(willDelete => {
      if (willDelete) {
        request = $.ajax({
          url: "../carrito/delete.php",
          type: "POST",
          data: { id: id }
        });
        preciototal();
        dibujarTabla();
      }
    });
  });

  $("#btn_check").click(function() {
    swal({
      title: "Hmm... Va bastante lleno !",
      text:
        'Al oprimir "CheckOut" se vaciara el carrito y se creara una nueva factura con la fecha de hoy.',
      icon: "warning",
      buttons: ["Cancelar", "CheckOut"]
    }).then(willDelete => {
      if (willDelete) {
        borrarCantidades();
        swal({
          title: "Factura Creada!",
          text: "Gracias por Comprar En LazzyCat",
          icon: "success",
          button: "Oh si!"
        });
        $.post("/carrito/checkout.php", function(data) {
          preciototal();
          dibujarTabla();
        });
        $.post("/cronjob/email.php", function(data) {
        });
      }
    });
  });

  $(document).on("click", ".editar", function(e) {
    var id = $(this).attr("id");
    //Editar el monto en la base de datos talvez con un hidden input
    var cant = $("#cantidad_producto" + id).val();
    var precio = $("#producto_precio" + id).val();
    var monto = precio * cant;
    var stock = $(this).attr("value");
    if (cant * 1 <= stock * 1) {
      $("#monto" + id).val(monto);
      request = $.ajax({
        url: "../carrito/cart_edit.php",
        type: "POST",
        data: { id: id, cant: cant, stock: stock, monto: monto }
      });
    } else {
      swal({
        title: "Lo Sentimos !",
        text: `De este Objeto solo tenemos en Stock ${stock}`,
        icon: "error",
        button: "Esta bien!"
      });
    }

    preciototal();
    dibujarTabla();
  });

  function preciototal() {
    ajaxRequestTotal().then(function(json) {
      json.forEach(function(item) {
        $(".subtotal_pago").val(item.total * 1 + 0);
        $(".total_pago").val(item.total * 1 + 5);
      });
    });
  }

  function borrarCantidades() {
    ajaxRequest().then(function(json) {
      json.forEach(function(item) {
        request = $.ajax({
          url: "../carrito/cantidad_ajax.php",
          type: "POST",
          data: { id: item.id, cant: item.cantidad }
        });
      });
    });
  }

  dibujarTabla();
});
