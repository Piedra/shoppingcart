<?php 
 $title = 'Registrar'; 
require_once './shared/header.php';
require_once './shared/sessions.php';
require_once './Models/usuario.php';
require_once './shared/db.php';
require_once './php_database/PgConnection.php';
$nomErr;
$apeErr;
$direErr;
$telErr;
$corrErr;
$passErr="";
?>


<div class='row'>
    <div class='col-md-6 col-md-offset-3'>
        <div class="panel panel-default">

            <div class="panel-heading">Sign Up</div>
            <div class="panel-body">
            <?php
             if(isset($_POST['nombre'],$_POST['apellidos'],$_POST['direccion'],$_POST['telefono'],
             $_POST['correo'],$_POST['password']))
             {   
                 $nombre     = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
                 $apellidos  = filter_input(INPUT_POST, 'apellidos', FILTER_SANITIZE_STRING);
                 $direccion  = filter_input(INPUT_POST, 'direccion', FILTER_SANITIZE_STRING);
                 $telefono   = filter_input(INPUT_POST, 'telefono', FILTER_SANITIZE_STRING);
                 $correo     = filter_input(INPUT_POST, 'correo', FILTER_SANITIZE_STRING);
                 $password   = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
                 $admin = 'false';
                 if ($usuario_model->validacion($correo)) {
                    $passErr = "El correo ya existe, por favor intente con otro";
                }else {
                    $result = $usuario_model->registrar($nombre,$apellidos,$direccion,$telefono,$correo,$password,$admin);
                if($result)
                    {
                    $_SESSION['usuario'] = $result[1];
                    header('Location: '.'./index.php');
                    }      
            }
        }
            ?>
                <form action="" method="post" autocomplete="off">

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input required type="text" name="nombre" id="nombre" class="form-control" value="<?=$nombre ?? ''?>">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group ">
                        <label for="apellidos">Apellidos</label>
                        <input required type="text" name="apellidos" id="apellidos" class="form-control" value="<?=$apellidos ?? ''?>">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group ">
                        <label for="direccion">Dirección</label>
                        <input  required type="text" name="direccion" id="direccion" class="form-control" value="<?=$direccion ?? ''?>">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group ">
                        <label for="telefono">Teléfono</label>
                        <input  required type="text" name="telefono" id="telefono" class="form-control" value="<?=$telefono ?? ''?>">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group">
                        <label for="correo">Correo Electrónico</label>
                        <input required type="email" name="correo" id="correo" placeholder="tu@dominio.com" class="form-control">
                        <span class="text-danger"> <?=$passErr?></span>
                    </div>

                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input required type="password" name="password" id="password" class="form-control">
                        <span class="help-block"></span>
                    </div>

                    <button type="submit" class="btn btn-default">Sign Up</button>


                </form>

            </div>

        </div>
    </div>

</div>