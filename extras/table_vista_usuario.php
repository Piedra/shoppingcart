<thead>
    <tr>
        <th class="titulos"><h1 >Total de Productos Adquiridos</h1></th>
    </tr>
    <tr>
    <td><h2 class="stadistic text-success" ><?= $cantidad_comprados['sum'] ?></h2></td>
    </tr>
    <tr>
        <th class="titulos"><h1>Monto de compra realizadas</h1></th>
    </tr>
    <tr>
    <td><h2 class="stadistic text-success" ><?= $monto['sum'] ?></h2></td>
    </tr>
</thead>