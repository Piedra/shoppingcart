<?php
$title = 'Acceso Denegado'; 
require_once '../shared/header.php';
require_once '../shared/sessions.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
</head>
<body>
    <div class="container col-sm-12">
        <h1 style="text-align: center ; font-size:400px;"class="stadistic text-info">404</h1>
        <h2 style="text-align: center ; font-size:60px;"class="text-danger">Acceso denegado, no cumple con los permisos</h2>
    </div>
</body>
</html>