<thead>
    <tr>
        <th class="titulos"><h1 >Clientes Registrados en la Aplicación</h1></th>
    </tr>
    <tr>
    <td><h2 class="stadistic text-info" ><?= $us_regitrados['count'] ?></h2></td>
    </tr>
    <tr>
        <th class="titulos"><h1>Cantidad de Productos Vendidos</h1></th>
    </tr>
    <tr>
    <td><h2 class="stadistic text-info" ><?= $prod_vendidos['sum'] ?></h2></td>
    </tr>
    <tr>
        <th class="titulos"><h1> Monto de todas las ventas Realizadas</h1></th>
    </tr>
    <tr>
    <td><h2 class="stadistic text-success" >$<?= $monto_total['sum'] ?></h2></td>
    </tr>
</thead>