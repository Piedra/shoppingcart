<?php $title = 'Iniciar Sesion'; 
 require_once  './shared/header.php';
 require_once './shared/sessions.php';
 require_once './shared/db.php';
 
 $email =  filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING) ?? '';
 $pass =  filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) ?? '';
 $result = $usuario_model->login($email,$pass);

 $nameErr = "";

if ($result) {
    foreach ($result as $res) {
        //$usu = new Usuario($res['id'], $res['nombre'], $res['apellidos'], $res['direccion'], $res['telefono'], $res['correo'], $res['password'], $res['administrador']);
        $_SESSION['usuario'] = [$res['id'], $res['nombre'], $res['apellidos'], $res['direccion'], $res['telefono'], $res['correo'], $res['password'], $res['administrador']];
    }
}else {
    $nameErr = "El correo o contraseña invalidos, intente nuevamente";
}

  $con->disconnect();

if (isset($_SESSION['usuario'])) {
    //echo "Bienvenido ". $_SESSION['usuario'];
    echo "<a href='../logout.php'>Cerrar Sesión</a>";
    header('Location: ../index.php');
    return;
}

?>
<div class='row'>
    <div class='col-md-6 col-md-offset-3'>
        <div class="panel panel-default">
            <div class="panel-heading">Sign In</div>
                <div class="panel-body">
                    <form method="post" action="" method="post" autocomplete="off">
                       <div class="form-group">
                           <label for="email">Email</label>
                           <input type="email" name="email" id="email" placeholder="tu@dominio.com"
                            class="form-control" value="">
                           <span class="help-block"></span>
                       </div>
                        <div class="form-group">
                           <label for="password">Password</label>
                           <input type="password" name="password" id="password" class="form-control">

                           <span class="help-block"></span>
                       </div>
                       <button type="submit" class="btn btn-default">Sign In</button><br>
                       <!--<span class="text-danger"> <?=$nameErr?></span>-->
                    </form>
                </div>
             </div>
        </div>
    </div>
</div>