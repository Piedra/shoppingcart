<?php
  $title = 'Home';
  require_once './shared/header.php';
  require_once './shared/sessions.php';
  require_once './shared/guard.php';
  require_once './shared/db.php';


  if($es_admin=="f"){
    header('Location: ./index2.php');
  }
  $us_regitrados = $usuario_model->cantidad();
  $prod_vendidos = $carrito_model->cantidad();
  $monto_total = $carrito_model->montoTotal();
  $prod_vendido = $carrito_model->masVendido();
  if(isset($prod_vendido)){
    $max = max($prod_vendido);
    $min = min($prod_vendido);
  }else{
    $max = 0;
    $min = 0;    
  }
  

?>
<!DOCTYPE html>
<html>
  <body>
    <html>
      <div class="container charts">
        <div class="col-sm-5 border border-info rounded-right">
          <table class="table">
            <tbody>
              <?php require_once './extras/table_index.php'?>
            </tbody>
          </table>
        </div>
        <div class="col-sm-5 border border-success rounded-right">
          <table class="table">
            <tbody>
              <?php require_once './extras/table_index_b.php'?>
            </tbody>
          </table>
        </div>
      </div>
    </html>
  </body>
</html>