<?php
$title = 'Carrito'; 
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';
//CodigoCart
$id = 0;
if(isset($_SESSION['usuario'])){
    $id = $_SESSION['usuario'][0];
}
?>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <div class="container" id="myCart">
        <div class="col-md-8">
            <div class="">
                <input type="hidden" id="id" value=<?=$id?>>
                <table  id="tabla_productos" style="text-align: center" class="table table-striped">
                <div id='response'></div>
                    <?php require './row.php'?>
                </table>
            </div>
        </div>
        <div class="col-md-4">
               <?php require './paying_info.php'?>
        </div>
    </div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../assets/js/load_cart.js"></script>
</body>
</html>