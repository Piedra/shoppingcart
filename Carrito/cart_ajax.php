<?php
$title = 'Carrito'; 
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$id        = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$productos = $carrito_model->select($id);
echo json_encode($productos);
