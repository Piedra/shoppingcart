<?php
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$cant = filter_input(INPUT_POST, 'cant', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

$carrito_model->updateCant($id,$cant);
$carrito_model->checkout();
$carrito_model->deleteAll();