<?php
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$total = $carrito_model->total();
echo json_encode($total);