<?php
$title = 'Info';  
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

    if(!isset($_SESSION['product_id'])){
        header('Location: ../productos/productsview.php'); 
    }

    $id_usu = $_SESSION['usuario'][0] ?? 0;
    $id = $_SESSION['product_id'];
    

$product = $productos_model->search_id($id);
$rand = rand(1,5);
?>
<!DOCTYPE html>
<html>
    <body>
        <div class="container" id="body_product">
            <div class="col-sm-6" id="product_id">
            <img  class="img-thumbnail" src="<?=$product['imagen']?>">
            </div>
            <div class="col-sm-4 container">
                <?php require_once './prod_info.php'?>
            </div>
            <div id="buyingCart" class="col-sm-2 container">
                <?php require_once './shop_view.php' ?>
            </div> <!--Fin del Div del del menu BuyingCart-->
        </div> <!--Fin del panel principal-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../assets/js/back_shop.js"></script>
    </body>
</html>