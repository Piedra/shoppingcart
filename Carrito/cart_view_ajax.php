<?php
$title = 'Info';  
require_once '../shared/sessions.php';
require_once '../php_database/PgConnection.php';
require_once '../shared/db.php';

$id_prod = filter_input(INPUT_POST, 'id_prod', FILTER_SANITIZE_STRING);
$id_usu = filter_input(INPUT_POST, 'id_usu', FILTER_SANITIZE_STRING);
$cantidad = filter_input(INPUT_POST, 'cant', FILTER_SANITIZE_STRING);
$precio = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);
$monto = $precio*$cantidad;

$carrito = $carrito_model->ingresar($id_prod,$id_usu,$cantidad,$monto);
echo($carrito);