<h1>
    <?= $product['nombre']?>
</h1>
    <br>
    <p class="product_info">Descripcion</p>
    <p class="product_text">
        <?= $product['descripcion']?>
    </p>
    <div class="product_row">
        <p class="product_info">Calidad:&nbsp;&nbsp;</p>
        <?php for($i=0; $i < $rand; $i++): ?>
        <p class="estrella"><i class="fas fa-star"></i></p>
        <?php endfor; ?>
    </div>
    <br>
    <div class="product_row">
        <p class="product_info">Precio: </p>
        <p class="product_price">&nbsp;$
            <?php echo $product['precio']?>
        </p>
    </div>